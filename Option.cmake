include_directories(${ROOT})
include_directories(Include)
include_directories(Include/metis)

set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Debug Release RelWithDebInfo MinSizeRel")

message("CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")

set(BUILD_PACKAGE "" CACHE STRING "DEB OR RPM")

option(BUILD_DLL_EXAMPLE "BUILD DYNAMIC LIBRARY EXAMPLE" OFF)
option(BUILD_MULTITHREAD "BUILD MULTI THREADED VERSION" OFF)
option(BUILD_SHARED "LINK ALL SHARED LIBRARY" OFF)
option(USE_SUPERLUMT "USE MULTI THREADED SUPERLU" OFF)
option(USE_EXTERNAL_VTK "USE EXTERNAL VTK LIBRARY TO ENABLE VISUALIZATION" OFF)
option(USE_HDF5 "ENABLE HDF5 SUPPORT TO RECORD DATA" ON)
option(USE_AVX "USE AVX" ON)
option(USE_AVX2 "USE AVX2" OFF)
option(USE_AVX512 "USE AVX512" OFF)
option(USE_MKL "USE INTEL MKL" OFF)
if (USE_MKL)
    option(USE_INTEL_OPENMP "USE INTEL OPENMP IMPLEMENTATION ON LINUX AND MACOS" ON)
    option(LINK_DYNAMIC_MKL "LINK DYNAMIC MKL LIBRARY" ON)
endif ()

set(COMPILER_IDENTIFIER "unknown")
set(SP_EXTERNAL_LIB_PATH "unknown")
if (CMAKE_SYSTEM_NAME MATCHES "Windows") # WINDOWS PLATFORM
    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU") # GNU GCC COMPILER
        set(COMPILER_IDENTIFIER "gcc-win")
        set(SP_EXTERNAL_LIB_PATH "win")
    elseif (CMAKE_CXX_COMPILER_ID MATCHES "MSVC" OR CMAKE_CXX_COMPILER_ID MATCHES "Intel") # MSVC COMPILER
        set(COMPILER_IDENTIFIER "vs")
        set(SP_EXTERNAL_LIB_PATH "vs")
        add_compile_definitions(_SILENCE_CXX17_ITERATOR_BASE_CLASS_DEPRECATION_WARNING)
        if (FORTRAN_STATUS)
            set(BUILD_SHARED OFF CACHE BOOL "" FORCE)
        endif ()
        option(USE_EXTERNAL_CUDA "USE EXTERNAL CUDA LIBRARY TO UTILIZE GPU" OFF)
    endif ()
elseif (CMAKE_SYSTEM_NAME MATCHES "Linux") # LINUX PLATFORM
    set(SP_EXTERNAL_LIB_PATH "linux")
    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU") # GNU GCC COMPILER
        set(COMPILER_IDENTIFIER "gcc-linux")
    elseif (CMAKE_CXX_COMPILER_ID MATCHES "IntelLLVM") # Intel COMPILER icx
        set(COMPILER_IDENTIFIER "clang-linux")
    elseif (CMAKE_CXX_COMPILER_ID MATCHES "Intel") # Intel COMPILER Classic icc
        set(COMPILER_IDENTIFIER "gcc-linux")
        message("Classic Intel compiler icc has incomplete CPP20 support, if it fails to compile please use another compiler.")
    elseif (CMAKE_CXX_COMPILER_ID MATCHES "Clang") # Clang COMPILER
        set(COMPILER_IDENTIFIER "clang-linux")
    endif ()
    option(USE_EXTERNAL_CUDA "USE EXTERNAL CUDA LIBRARY TO UTILIZE GPU" OFF)
elseif (CMAKE_SYSTEM_NAME MATCHES "Darwin") # MAC PLATFORM
    set(SP_EXTERNAL_LIB_PATH "mac")
    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU") # GNU GCC COMPILER
        set(COMPILER_IDENTIFIER "gcc-mac")
    elseif (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        set(COMPILER_IDENTIFIER "clang-mac")
        link_directories(/usr/local/opt/libomp/lib/)
        message("On macOS, make sure llvm and libomp are installed.")
        message("brew install llvm libomp")
    endif ()
endif ()

if (COMPILER_IDENTIFIER MATCHES "unknown")
    message(FATAL_ERROR "Cannot identify the compiler available, please use GCC or MSVC or Intel.")
endif ()

link_directories(Libs/${SP_EXTERNAL_LIB_PATH})

if (USE_SUPERLUMT)
    add_compile_definitions(SUANPAN_SUPERLUMT)
endif ()

if (USE_MKL)
    set(MKLROOT "" CACHE PATH "MKL library path which contains /include and /lib folders.")
    find_file(MKL_HEADER NAMES mkl.h PATHS ${MKLROOT}/include)
    if (MKL_HEADER MATCHES "MKL_HEADER-NOTFOUND")
        message(FATAL_ERROR "The mkl.h is not found under the path: ${MKLROOT}/include")
    endif ()
    add_compile_definitions(SUANPAN_MKL)
    add_compile_definitions(ARMA_USE_MKL_ALLOC)
    include_directories(${MKLROOT}/include)
    link_directories(${MKLROOT}/lib/intel64)
    if (COMPILER_IDENTIFIER MATCHES "IntelLLVM")
        set(USE_INTEL_OPENMP ON CACHE BOOL "" FORCE)
    endif ()
    if (USE_INTEL_OPENMP)
        if (MKLROOT MATCHES "(oneapi|oneAPI)")
            if (COMPILER_IDENTIFIER MATCHES "linux")
                find_library(IOMPPATH iomp5 ${MKLROOT}/../../compiler/latest/linux/compiler/lib/intel64_lin)
                get_filename_component(IOMPPATH ${IOMPPATH} DIRECTORY)
                link_directories(${IOMPPATH})
            elseif (COMPILER_IDENTIFIER MATCHES "(win|vs)")
                find_library(IOMPPATH libiomp5md ${MKLROOT}/../../compiler/latest/windows/compiler/lib/intel64_win)
                get_filename_component(IOMPPATH ${IOMPPATH} DIRECTORY)
                link_directories(${IOMPPATH})
            endif ()
        else ()
            message(FATAL_ERROR "Intel Parallel Studio is not supported, please install Intel oneAPI toolkits.")
        endif ()
    endif ()
endif ()

if (USE_EXTERNAL_CUDA)
    find_package(CUDA)
    if (NOT CUDA_FOUND)
        set(CUDA_PATH "" CACHE PATH "CUDA library path which contains /include folder")
        find_package(CUDA PATHS ${CUDA_PATH})
        if (NOT CUDA_FOUND)
            message(FATAL_ERROR "CUDA library is not found, please indicate its path.")
        endif ()
    endif ()
    add_compile_definitions(SUANPAN_CUDA)
    include_directories(${CUDA_INCLUDE_DIRS})
    link_libraries(${CUDA_LIBRARIES} ${CUDA_CUBLAS_LIBRARIES} ${CUDA_cusolver_LIBRARY} ${CUDA_cusparse_LIBRARY})
endif ()

set(HAVE_VTK FALSE CACHE INTERNAL "")
if (USE_EXTERNAL_VTK)
    if (VTK_PATH MATCHES "")
        find_package(VTK)
    else ()
        find_package(VTK PATHS ${VTK_PATH})
    endif ()
    if (VTK_FOUND)
        add_compile_definitions(SUANPAN_VTK)
        set(HAVE_VTK TRUE CACHE INTERNAL "")
    else ()
        set(VTK_PATH "" CACHE PATH "VTK library path which contains /include folder")
        find_package(VTK PATHS ${VTK_PATH})
        if (NOT VTK_FOUND)
            message(FATAL_ERROR "VTK library is not found, please indicate its path.")
        endif ()
    endif ()
endif ()

if (USE_HDF5)
    add_compile_definitions(SUANPAN_HDF5)
    include_directories(Include/hdf5)
    include_directories(Include/hdf5-${SP_EXTERNAL_LIB_PATH})
    if (COMPILER_IDENTIFIER MATCHES "vs")
        link_libraries(libhdf5_hl libhdf5)
    else ()
        link_libraries(hdf5_hl hdf5)
    endif ()
else ()
    add_compile_definitions(ARMA_DONT_USE_HDF5)
endif ()

if (BUILD_MULTITHREAD)
    message("USING TBB LIBRARY")
    add_compile_definitions(SUANPAN_MT)
    if (COMPILER_IDENTIFIER MATCHES "gcc-win")
        link_libraries(tbb12)
    else ()
        link_libraries(tbb)
    endif ()
endif ()

if (BUILD_SHARED)
    message("BUILD SHARED LIBRARY")
    set(LIBRARY_TYPE SHARED)
else ()
    message("BUILD STATIC LIBRARY")
    set(LIBRARY_TYPE STATIC)
endif ()

if (COMPILER_IDENTIFIER MATCHES "vs")
    unset(TEST_COVERAGE CACHE)

    link_directories(Libs/win)

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP /openmp /EHsc")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /MP /openmp /EHsc")
    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} /MP /Qopenmp /Qparallel /fpp /names:lowercase /assume:underscore")
    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} /libs:dll /threads")

    if (USE_AVX512)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /arch:AVX512")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /arch:AVX512")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} /arch:AVX")
    elseif (USE_AVX2)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /arch:AVX2")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /arch:AVX2")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} /arch:AVX")
    elseif (USE_AVX)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /arch:AVX")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /arch:AVX")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} /arch:AVX")
    endif ()
else ()
    if (BUILD_SHARED)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -fPIC")
    endif ()

    link_libraries(dl pthread gfortran quadmath)

    if (CMAKE_CXX_COMPILER_ID MATCHES "IntelLLVM")
        link_libraries(stdc++)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ffp-model=precise -fexceptions -fiopenmp")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ffp-model=precise -fexceptions -fiopenmp")
    elseif (COMPILER_IDENTIFIER MATCHES "clang-mac")
        include_directories("/usr/local/include" "/usr/local/opt/llvm/include")
        link_directories("/usr/local/lib" "/usr/local/opt/llvm/lib")
        link_libraries(omp)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fexceptions -Xpreprocessor -fopenmp")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fexceptions -Xpreprocessor -fopenmp")
    else ()
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fexceptions -fopenmp")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fexceptions -fopenmp")
    endif ()

    if (USE_AVX512)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mavx512f")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mavx512f")
    elseif (USE_AVX2)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mavx2")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mavx2")
    elseif (USE_AVX)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mavx")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mavx")
    endif ()

    option(TEST_COVERAGE "TEST CODE COVERAGE USING GCOV" OFF)

    if (TEST_COVERAGE AND COMPILER_IDENTIFIER MATCHES "gcc") # only report coverage with gcc
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
        link_libraries(gcov)
    endif ()

    if (CMAKE_BUILD_TYPE MATCHES "Debug")
        option(USE_ASAN "USE ADDRESS SANITIZER" OFF)
        if (USE_ASAN)
            message(STATUS "Using the address sanitizer with flags: -fsanitize=address,leak,undefined")
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address,leak,undefined")
            set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fsanitize=address,leak,undefined")
        endif ()
    endif ()

    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -w -fallow-argument-mismatch")
    if (CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -fpp -qopenmp")
    else ()
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -cpp -fopenmp")
    endif ()
endif ()

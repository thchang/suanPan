/*******************************************************************************
 * Copyright (C) 2017-2022 Theodore Chang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

#include "BatheTwoStep.h"
#include <Domain/DomainBase.h>
#include <Domain/Factory.hpp>
#include <Domain/Node.h>

BatheTwoStep::BatheTwoStep(const unsigned T, const double R, const double G)
    : Integrator(T)
    , GM(G)
    , Q1((R + 1) / (2. * GM * (R - 1) + 4))
    , Q2(.5 - GM * Q1)
    , Q0(1. - Q1 - Q2) {}

void BatheTwoStep::assemble_resistance() {
    const auto& D = get_domain().lock();
    auto& W = D->get_factory();

    auto fa = std::async([&] { D->assemble_resistance(); });
    auto fb = std::async([&] { D->assemble_damping_force(); });
    auto fc = std::async([&] { D->assemble_inertial_force(); });

    fa.get();
    fb.get();
    fc.get();

    W->set_sushi(W->get_trial_resistance() + W->get_trial_damping_force() + W->get_trial_inertial_force());
}

void BatheTwoStep::assemble_matrix() {
    const auto& D = get_domain().lock();
    auto& W = D->get_factory();

    auto fa = std::async([&] { D->assemble_trial_stiffness(); });
    auto fb = std::async([&] { D->assemble_trial_geometry(); });
    auto fc = std::async([&] { D->assemble_trial_damping(); });
    auto fd = std::async([&] { D->assemble_trial_mass(); });

    fa.get();
    fb.get();
    fc.get();
    fd.get();

    auto& t_stiff = W->get_stiffness();

    t_stiff += W->get_geometry();

    t_stiff += FLAG::TRAP == step_flag ? P3 * W->get_mass() + P2 * W->get_damping() : P9 * W->get_mass() + P8 * W->get_damping();
}

void BatheTwoStep::update_incre_time(double T) {
    const auto& W = get_domain().lock()->get_factory();
    update_parameter(T *= 2.);
    W->update_incre_time(T * (FLAG::TRAP == step_flag ? GM : 1. - GM));
}

int BatheTwoStep::update_trial_status() {
    const auto& D = get_domain().lock();

    if(auto& W = D->get_factory(); FLAG::TRAP == step_flag) {
        W->update_trial_acceleration(P3 * W->get_incre_displacement() - P4 * W->get_current_velocity() - W->get_current_acceleration());
        W->update_trial_velocity(P2 * W->get_incre_displacement() - W->get_current_velocity());
    }
    else {
        W->update_trial_velocity(P8 * (W->get_trial_displacement() - W->get_pre_displacement()) - Q02 * W->get_pre_velocity() - Q12 * W->get_current_velocity());
        W->update_trial_acceleration(P8 * (W->get_trial_velocity() - W->get_pre_velocity()) - Q02 * W->get_pre_acceleration() - Q12 * W->get_current_acceleration());
    }

    return D->update_trial_status();
}

void BatheTwoStep::commit_status() {
    const auto& D = get_domain().lock();
    auto& W = D->get_factory();

    if(FLAG::TRAP == step_flag) {
        step_flag = FLAG::EULER;
        set_time_step_switch(false);
    }
    else {
        step_flag = FLAG::TRAP;
        set_time_step_switch(true);
    }

    W->commit_pre_displacement();
    W->commit_pre_velocity();
    W->commit_pre_acceleration();

    Integrator::commit_status();
}

void BatheTwoStep::clear_status() {
    step_flag = FLAG::TRAP;
    set_time_step_switch(true);

    Integrator::clear_status();
}

/**
 * \brief update acceleration and velocity for zero displacement increment
 */
void BatheTwoStep::update_compatibility() const {
    const auto& D = get_domain().lock();
    auto& W = D->get_factory();

    if(FLAG::TRAP == step_flag) {
        W->update_trial_acceleration(-P4 * W->get_current_velocity() - W->get_current_acceleration());
        W->update_trial_velocity(-W->get_current_velocity());
    }
    else {
        W->update_trial_velocity(P8 * (W->get_current_displacement() - W->get_pre_displacement()) - Q02 * W->get_pre_velocity() - Q12 * W->get_current_velocity());
        W->update_trial_acceleration(P8 * (W->get_trial_velocity() - W->get_pre_velocity()) - Q02 * W->get_pre_acceleration() - Q12 * W->get_current_acceleration());
    }

    auto& trial_dsp = W->get_trial_displacement();
    auto& trial_vel = W->get_trial_velocity();
    auto& trial_acc = W->get_trial_acceleration();

    suanpan::for_all(D->get_node_pool(), [&](const shared_ptr<Node>& t_node) { t_node->update_trial_status(trial_dsp, trial_vel, trial_acc); });
}

vec BatheTwoStep::from_incre_velocity(const vec& incre_velocity, const uvec& encoding) {
    auto& W = get_domain().lock()->get_factory();

    return from_total_velocity(W->get_current_velocity()(encoding) + incre_velocity, encoding);
}

vec BatheTwoStep::from_incre_acceleration(const vec& incre_acceleration, const uvec& encoding) {
    auto& W = get_domain().lock()->get_factory();

    return from_total_acceleration(W->get_current_acceleration()(encoding) + incre_acceleration, encoding);
}

vec BatheTwoStep::from_total_velocity(const vec& total_velocity, const uvec& encoding) {
    auto& W = get_domain().lock()->get_factory();

    if(FLAG::TRAP == step_flag) return W->get_current_displacement()(encoding) + P1 * (W->get_current_velocity()(encoding) + total_velocity);

    return W->get_pre_displacement()(encoding) + P5 * W->get_pre_velocity()(encoding) + P6 * W->get_current_velocity()(encoding) + P7 * total_velocity;
}

vec BatheTwoStep::from_total_acceleration(const vec& total_acceleration, const uvec& encoding) {
    auto& W = get_domain().lock()->get_factory();

    vec total_velocity;
    if(FLAG::TRAP == step_flag) total_velocity = W->get_current_velocity()(encoding) + P1 * (W->get_current_acceleration()(encoding) + total_acceleration);
    else total_velocity = W->get_pre_velocity()(encoding) + P5 * W->get_pre_acceleration()(encoding) + P6 * W->get_current_acceleration()(encoding) + P7 * total_acceleration;

    return from_total_velocity(total_velocity, encoding);
}

void BatheTwoStep::update_parameter(const double NT) {
    if(suanpan::approx_equal(P0, NT)) return;

    P0 = NT;

    P1 = .5 * P0 * GM;
    P2 = 1. / P1;
    P3 = P2 * P2;
    P4 = 2. * P2;

    P5 = P0 * Q0;
    P6 = P0 * Q1;
    P7 = P0 * Q2;
    P8 = 1. / P7;
    P9 = P8 * P8;
}

void BatheTwoStep::print() { suanpan_info("A BatheTwoStep solver.\n"); }

---
name: Feature request
about: Suggest an idea for this project
title: "[FEATURE]"
labels: ''
assignees: ''

---

**Feature**
Please provide a concise description of the requested feature and how it would be applied to practical problems.

**References**
Please provide any relevant references (papers, thesis, etc.)
